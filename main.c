/*
** my_test.c for  in /home/eax/dev/jack_dev
** 
** Made by kevin soules
** Login   <soules_k@epitech.net>
** 
** Started on  Sun Nov  3 19:49:53 2013 kevin soules
** Last update Mon Nov  4 02:02:14 2013 kevin soules
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <signal.h>
#include <jack/jack.h>
#include <init.h>
#include <misc.h>
#include <amp.h>
#include <effect_functions.h>

static jack_client_t	*client_gl;

void	signal_handler(int sig)
{
  jack_client_close(client_gl);
  fprintf(stderr, "signal %d received, exiting ...\n", sig);
  exit(0);
}

int			main(int ac, char **av)
{
  t_amp	amp;

  amp.client_name = "my_amp";
  init_amp(&amp);
  client_gl = amp.client;

  signal(SIGQUIT, signal_handler);
  signal(SIGTERM, signal_handler);
  signal(SIGHUP, signal_handler);
  signal(SIGINT, signal_handler);

  init_effect(&amp);

  while (1)
    sleep(1);

  jack_client_close(amp.client);
  exit(0);
}
