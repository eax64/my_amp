/*
** effect.c for  in /home/eax/dev/jack_dev/amp
** 
** Made by kevin soules
** Login   <soules_k@epitech.net>
** 
** Started on  Mon Nov  4 01:16:45 2013 kevin soules
** Last update Mon Nov  4 01:52:35 2013 kevin soules
*/

#include <stdlib.h>
#include <misc.h>
#include <amp.h>

t_effect	*new_effect(t_effect_cb f, void *data)
{
  t_effect	*p;

  if (!(p = malloc(sizeof(*p))))
    pexit("Malloc error");

  p->f = f;
  p->data = data;
  p->next = NULL;;

  return (p);
}


t_effect	*add_effect(t_amp *amp, t_effect_cb f, void *data)
{
  t_effect      *p;

  if (!amp->effect)
    return (amp->effect = new_effect(f, data));

  p = amp->effect;
  while (p->next)
    p = p->next;

  return (p->next = new_effect(f, data));
}
