/*
** effect_functions.h for  in /home/eax/dev/jack_dev/amp
** 
** Made by kevin soules
** Login   <soules_k@epitech.net>
** 
** Started on  Mon Nov  4 01:34:42 2013 kevin soules
** Last update Mon Nov  4 02:58:26 2013 kevin soules
*/

#ifndef EFFECT_FUNCTION_H_
#define EFFECT_FUNCTION_H_

#include <amp.h>

#ifndef M_PI
#define M_PI  (3.14159265)
#endif

typedef struct
{
  float	level;
  float	val1;
} t_generic_config;

void	init_effect(t_amp *amp);

int	amp_effect_tremblo_cb(t_effect_data *data);
int	amp_effect_drive_cb(t_effect_data *data);
int	amp_effect_disto_cb(t_effect_data *data);
#endif
