/*
** jack_cb.c for  in /home/eax/dev/jack_dev/amp
** 
** Made by kevin soules
** Login   <soules_k@epitech.net>
** 
** Started on  Mon Nov  4 00:32:15 2013 kevin soules
** Last update Mon Nov  4 01:55:26 2013 kevin soules
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <jack/jack.h>
#include <jack_cb.h>
#include <amp.h>
#include <misc.h>
#include <effect.h>

void	jack_shutdown_cb(void __attribute__((unused))*arg)
{
  exit(1);
}

int	sample_rate_cb(jack_nframes_t nframes, void *arg)
{
  t_amp *data;

  data = arg;
  printf("The sample rate is now %lu/sec\n", (unsigned long)nframes);
  data->sample_rate = nframes;
  return (0);
}

int		process_cb(jack_nframes_t nframes, void *arg)
{
  t_amp		*amp;
  sample_t	*in;
  sample_t	*tmp_out;
  sample_t	*out;
  t_effect	*p;
  t_effect_data	d;

  amp = arg;
  in = (sample_t *)jack_port_get_buffer(amp->input_port, nframes);
  out = (sample_t *)jack_port_get_buffer(amp->output_port, nframes);

  if (!(tmp_out = malloc(sizeof(*tmp_out) * nframes)))
    pexit("Malloc error");

  memcpy(tmp_out, in, sizeof(sample_t) * nframes);

  p = amp->effect;
  if (!p)
    memcpy(out, in, sizeof(sample_t) * nframes);

  while (p)
    {
      d.in = tmp_out;
      d.out = out;
      d.data = p->data;
      d.nframes = nframes;
      d.sample_rate = amp->sample_rate;
      p->f(&d);
      p = p->next;
      if (p)
	memcpy(tmp_out, out, sizeof(sample_t) * nframes);
    }

  return (0);      
}
