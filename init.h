/*
** init.h for  in /home/eax/dev/jack_dev/amp
** 
** Made by kevin soules
** Login   <soules_k@epitech.net>
** 
** Started on  Sun Nov  3 23:55:47 2013 kevin soules
** Last update Mon Nov  4 00:40:10 2013 kevin soules
*/

#ifndef INIT_H_
#define INIT_H_

#include <jack/jack.h>
#include <amp.h>

jack_client_t	*init_client(char **client_name, jack_options_t options);
void	init_port(jack_client_t *client,
		    jack_port_t **output_port, jack_port_t **input_port);
void	init_amp(t_amp *amp);

#endif
