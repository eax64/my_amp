/*
** misc.c for  in /home/eax/dev/jack_dev/amp
** 
** Made by kevin soules
** Login   <soules_k@epitech.net>
** 
** Started on  Mon Nov  4 00:05:11 2013 kevin soules
** Last update Mon Nov  4 00:05:39 2013 kevin soules
*/

#include <stdio.h>
#include <stdlib.h>

void	pexit(char *m)
{
  fprintf(stderr, "%s\n", m);
  exit(EXIT_FAILURE);
}
