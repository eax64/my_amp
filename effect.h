/*
** effect.h for  in /home/eax/dev/jack_dev/amp
** 
** Made by kevin soules
** Login   <soules_k@epitech.net>
** 
** Started on  Mon Nov  4 00:41:42 2013 kevin soules
** Last update Mon Nov  4 01:48:07 2013 kevin soules
*/

#ifndef EFFECT_H_
#define EFFECT_H_

#include <amp.h>

typedef struct
{
  sample_t		*in;
  sample_t		*out;
  void			*data;
  jack_nframes_t	nframes;
  jack_nframes_t	sample_rate;
}			t_effect_data;

typedef int (*t_effect_cb) (t_effect_data*);

typedef struct s_effect
{
  t_effect_cb		f;
  void			*data;
  struct s_effect	*next;
}			t_effect;

t_effect	*add_effect(t_amp *amp, t_effect_cb, void *data);

#endif
