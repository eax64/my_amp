/*
** jack_cb.h for  in /home/eax/dev/jack_dev/amp
** 
** Made by kevin soules
** Login   <soules_k@epitech.net>
** 
** Started on  Mon Nov  4 00:34:16 2013 kevin soules
** Last update Mon Nov  4 00:36:13 2013 kevin soules
*/

#ifndef JACK_CB_H
#define JACK_CB_H

#include <jack/jack.h>

void	jack_shutdown_cb(void __attribute__((unused))*arg);
int	sample_rate_cb(jack_nframes_t nframes, void *arg);
int	process_cb(jack_nframes_t nframes, void *arg);

#endif
