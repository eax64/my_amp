##
## Makefile<2> for  in /home/eax/dev/jack_dev/amp
## 
## Made by kevin soules
## Login   <soules_k@epitech.net>
## 
## Started on  Sun Nov  3 23:49:02 2013 kevin soules
## Last update Mon Nov  4 10:58:30 2013 kevin soules
##

NAME	= amp
CC	= gcc

SRC	= main.c \
	  init.c \
	  misc.c \
	  jack_cb.c \
	  effect.c \
	  effect/init_effect.c \
	  effect/tremblo.c \
	  effect/drive.c \
	  effect/disto.c

RM	= rm -f

OBJ	=  $(SRC:.c=.o)

CFLAGS	= -ansi -pedantic -W -Wall -Wextra -O2 -I.

LFLAGS	= -ljack -lm

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(LFLAGS)

clean:
	$(RM) $(OBJ)

fclean:	clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
