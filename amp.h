/*
** amp.h for  in /home/eax/dev/jack_dev/amp
** 
** Made by kevin soules
** Login   <soules_k@epitech.net>
** 
** Started on  Mon Nov  4 00:01:59 2013 kevin soules
** Last update Mon Nov  4 01:35:53 2013 kevin soules
*/

#ifndef AMP_H_
#define AMP_H_

#include <jack/jack.h> 

typedef jack_default_audio_sample_t sample_t;
typedef struct s_amp t_amp;

#include <effect.h> 


struct s_amp
{
  char			*client_name;
  jack_client_t		*client;
  jack_port_t		*output_port;
  jack_port_t		*input_port;
  jack_nframes_t	sample_rate;
  t_effect		*effect;
};

#endif
