/*
** init.c for  in /home/eax/dev/jack_dev/amp
** 
** Made by kevin soules
** Login   <soules_k@epitech.net>
** 
** Started on  Sun Nov  3 23:35:34 2013 kevin soules
** Last update Mon Nov  4 01:09:51 2013 kevin soules
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <signal.h>
#include <jack/jack.h>
#include <misc.h>
#include <jack_cb.h>
#include <amp.h>

jack_client_t	*init_client(char **client_name, jack_options_t options)
{
  jack_client_t	*client;
  jack_status_t	status;

  client = jack_client_open(*client_name, options, &status, NULL);
  if (!client)
    pexit("jack_client_open() error");
  if (status & JackServerFailed)
    pexit("Unable to connect to JACK server");
  if (status & JackServerStarted)
    printf("JACK server started\n");
  if (status & JackNameNotUnique)
    {
      *client_name = jack_get_client_name(client);
      printf("Client name changed by JACK (not unique): %s\n", *client_name);
    }
  return (client);
}

void	init_port(jack_client_t *client,
		  jack_port_t **output_port, jack_port_t **input_port)
{
  *output_port = jack_port_register(client, "output1",
				   JACK_DEFAULT_AUDIO_TYPE,
				   JackPortIsOutput, 0);

  *input_port = jack_port_register(client, "input1",
				  JACK_DEFAULT_AUDIO_TYPE,
				  JackPortIsInput, 0);

  if (!*output_port || !*input_port)
    pexit("No more JACK ports available");
}

void	init_amp(t_amp *amp)
{
  amp->effect = NULL;

  amp->client = init_client(&amp->client_name, JackNullOption);
  init_port(amp->client, &amp->output_port, &amp->input_port);

  amp->sample_rate = jack_get_sample_rate(amp->client);

  jack_set_process_callback(amp->client, process_cb, amp);
  jack_on_shutdown(amp->client, jack_shutdown_cb, NULL);
  jack_set_sample_rate_callback(amp->client, sample_rate_cb, amp);

  if (jack_activate(amp->client))
    pexit("Cannot activate client");
}
