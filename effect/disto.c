/*
** disto.c for  in /home/eax/dev/jack_dev/amp
** 
** Made by kevin soules
** Login   <soules_k@epitech.net>
** 
** Started on  Mon Nov  4 02:52:02 2013 kevin soules
** Last update Mon Nov  4 11:00:06 2013 kevin soules
*/

#include <effect_functions.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <amp.h>

int			amp_effect_disto_cb(t_effect_data *data)
{
  size_t		i;
  float			x;
  float			val;
  t_generic_config	*cfg;

  cfg = data->data;
  for (i = 0; i < data->nframes; i++)
    {
      val = data->in[i];
      if (val > cfg->level)
      	x = cfg->level;
      else if (val < -cfg->level)
      	x = -cfg->level;
      else
      	x = val;

      data->out[i] = val * x;
    }
  return (0);
}
