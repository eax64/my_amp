/*
** init_effect.c for  in /home/eax/dev/jack_dev/amp
** 
** Made by kevin soules
** Login   <soules_k@epitech.net>
** 
** Started on  Mon Nov  4 01:31:12 2013 kevin soules
** Last update Mon Nov  4 03:33:35 2013 kevin soules
*/

#include <stdlib.h>
#include <misc.h>
#include <amp.h>
#include <effect_functions.h>

t_generic_config	*make_generic_config(float level, float val1)
{
  t_generic_config	*p;

  if (!(p = malloc(sizeof(*p))))
      pexit("Malloc error");
  p->level = level;
  p->val1 = val1;
  return (p);
}

void	init_effect(t_amp *amp)
{
  /* add_effect(amp, amp_effect_tremblo_cb, make_generic_config(0.3, 2)); */
  /* add_effect(amp, amp_effect_drive_cb, make_generic_config(40, 0)); */
  add_effect(amp, amp_effect_disto_cb, make_generic_config(0.3, 3.33));
}
