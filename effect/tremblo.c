/*
** tremblo.c for  in /home/eax/dev/jack_dev/amp
** 
** Made by kevin soules
** Login   <soules_k@epitech.net>
** 
** Started on  Mon Nov  4 01:37:45 2013 kevin soules
** Last update Mon Nov  4 02:51:17 2013 kevin soules
*/

#include <effect_functions.h>
#include <stdio.h>
#include <math.h>
#include <amp.h>

static unsigned int		global_cnt = 0;

int			amp_effect_tremblo_cb(t_effect_data *data)
{
  size_t		i;
  float			s;
  t_generic_config	*cfg;

  cfg = data->data;
  for (i = 0; i < data->nframes; i++)
    {
      s = sin((global_cnt++) * ((2 * M_PI) / (float)(data->sample_rate/cfg->val1)));
      s = s * (1 - cfg->level) + cfg->level;
      data->out[i] = data->in[i] * s;
    }
  return (0);
}
