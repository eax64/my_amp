/*
** drive.c for  in /home/eax/dev/jack_dev/amp
** 
** Made by kevin soules
** Login   <soules_k@epitech.net>
** 
** Started on  Mon Nov  4 02:24:13 2013 kevin soules
** Last update Mon Nov  4 02:39:28 2013 kevin soules
*/

#include <effect_functions.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <amp.h>

int			amp_effect_drive_cb(t_effect_data *data)
{
  size_t		i;
  float			x;
  float			k;
  float			a;
  t_generic_config	*cfg;

  cfg = data->data;
  for (i = 0; i < data->nframes; i++)
    {
      x = data->in[i];
      a = sin(((cfg->level + 1) / 100) * (M_PI/2));
      k = 2 * a / (1 - a);
      x = (1 + k) * x/ (1 + k * abs(x));
      if (x > 1)
	x = 1;
      else if (x < -1)
	x = -1;
      data->out[i] = x;
    }
  return (0);
}
